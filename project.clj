(defproject consul "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [me.raynes/conch "0.8.0"]
                 [enlive "1.1.6"]
                 [mysql/mysql-connector-java "8.0.16"]
                 [org.postgresql/postgresql "42.2.5.jre7"]
                 [rasta "0.1.0-SNAPSHOT"]
                 [toucan "1.12.0"]]
    :jvm-opts ["-Xmx10g"]
  :repl-options {:init-ns consul.core})
