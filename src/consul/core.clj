(ns consul.core
  (:require [clojure.string :as str]
            [me.raynes.conch :refer [programs]]
            [digitalize.core :refer :all]
            [formaterr.core :refer :all]
            [clojure.java.jdbc :as j]
            [mongerr.core :refer :all]
            [toucan
             [db :as db]
             [models :as models]]))

;; Prender metabase
;; java -jar ~/metabase/metabase.jar

;; Tuneles
(programs ssh)

;;  para postgres:
; ssh -L 5432:pcuervo-sphinx-postgresql:5432 pcuervo-sphinx-postgresql
;; 35.225.65.246	pcuervo-sphinx-postgresql

;; para mariadb
;; ssh propsmaria -L 3306:pcuervo-super-db1:3306

(def postgres
  {;:classname "org.postgresql.Driver"
   ;:subprotocol "postgresql"
   ;:subname "//pcuervo-sphinx-postgresql:5432/propiedades"
   :dbtype   "postgresql"
   :dbname   "propiedades"
   :host     "pcuervo-sphinx-postgresql"; "localhost";
   :user     "postgres"
   :password "fBQ=VmtQXf]8BC#6+C6"})

(db/set-default-db-connection! postgres)

(models/defmodel Catastro :catastros)
(models/defmodel Poligonos_Predios :poligonos_predios) ;; se puede poner como poligonos-predios y reemplaza - -> _ ?
;;(def c  (db/select Catastro :street [:= "HUICHAPAN"])) ;(db/select Catastro :street "HUICHAPAN")

(defn polygon-by-id [id] (db/select Catastro :polygon_id id))

(def maria
  {:dbtype   "mysql"
   :dbname   "propiedades"
   :host     "localhost";"pcuervo-super-db1"
   :user     "root"
   :password "propiedades"})

(def maria {:dbtype "mysql" :connection-uri "jdbc:mysql://propiedades:propiedades@localhost:3306/propiedades?serverTimezone=GMT"})

(models/defmodel Sepomex :sepomex)
(defn sepomex-by-polygon-id [id] (j/query maria ["select * from sepomex where id_polygon = ?" id]))
(defn todo-sepomex [] (j/query maria ["select * from sepomex"]))

;(defn sepomex-by-polygon-id [id] (binding [db/*db-connection* maria] (db/select Sepomex :id_polygon id)))
;(defn sepomex-by-polygon-id [id] (db/select Sepomex :id_polygon id))
;(j/query maria
;  ["select * from sepomex where id_polygon = ?" "313117"])

;; Catastro CDMX
;; csv     https://datos.cdmx.gob.mx/explore/dataset/uo-de-suelo/download/?format=csv&timezone=America/Mexico_City&use_labels_for_header=true
;; json    https://datos.cdmx.gob.mx/explore/dataset/uo-de-suelo/download/?format=json&timezone=America/Mexico_City
;; geojson https://datos.cdmx.gob.mx/explore/dataset/uo-de-suelo/download/?format=geojson&timezone=America/Mexico_City
;(def cdmx-csv (csv "resources/uo-de-suelo.csv")) ;; el separador es ; y no ,
(defn json-cdmx [] (:features (json (slurp "resources/uo-de-suelo.json"))))

(defn coords-json []
  (set (map #(vector (:latitude %) (:longitude %)) (json-cdmx)))) ;; 1216028

(defn coords-sepomex []
  (set (map #(vector (:lat %) (:lon %)) (todo-sepomex))))  ; coords:58193  todo-sepomex:132133

;;(def ppredios (:poligonos-predios (json (slurp "/home/nex/propiedades-consul/resources/poligonos_predios/poligonos_predios_201906182303.json"))))
;;(db-insert :poligonos-predios ppredios
;;(def ctstrs (:catastros (json (slurp "/home/nex/propiedades-consul/resources/catastros/catastros_201906182222.json"))))
;;(def sepmx (:sepomex (json (slurp "/home/nex/propiedades-consul/resources/sepomex_201906182343.json"))))

(defn igualdad-digital? [a b]
  (try (= (str/lower-case (digitalize a)) (str/lower-case (digitalize b)))
       (catch Exception e false)))

(def sepmx (db :sepomex))

(defn find-sepomex  ;; en datos abiertos las colonias vienen en mayusculas
  [m]
  (filter #(and (igualdad-digital? (:colony %) (-> m :properties :colonia))
                (= (:zipcode %) (-> m :properties :codigo-postal)))
          sepmx))

(defn transform-account [account]
  (str/replace (if (= (first account) \0) (apply str (rest account)))
               #"_" ""))

(defn parseo-seguro [o]
  (if-not (= "NULL" o)
    (digitalize o)))

(defn transform-catastro [predio]
  (try
    (let [sep (first (find-sepomex predio))]
      {:account (transform-account (-> predio :properties :cuenta-catastral))
       :street (str/capitalize (-> predio :properties :calle)) ;; Capitalizar o sepomex
       :number (-> predio :properties :no-externo)
       :zipcode (-> predio :properties :codigo-postal)
       :colony (:colony sep) ;(-> predio :properties :colonia) ;; Capitalizar o sepomex
       :city (:city sep) ;(-> predio :properties :alcaldia)
       :state "Ciudad de México" ;; TODO validar esta bien o DF?
       :sepomex_id (:id sep)
       :city_id (:city_id sep)
       :state_id (:state_id sep)
       :latitude (-> predio :properties :latitude)
       :longitude (-> predio :properties :longitude)
       :density (-> predio :properties :densidad-descripcion)  ;; este se puede parsear mejor?
       ;;:feasibilities                                               ;; TODO: parsear la ficha
       :height (parseo-seguro (-> predio :properties :altura))        ;; validar
       ;;:image_polygon
       :land_use (-> predio :properties :uso-descripcion)
       :levels (parseo-seguro (-> predio :properties :niveles))       ;; validar
       :free_area (parseo-seguro (-> predio :properties :area-libre)) ;; validar
       })
    (catch Exception e (println "exception " e " en " predio))))

(defn genera-nuevos-catastros []
  (db-insert :catastros-new (map transform-catastro (db :cdmx))))

(defn insert-catastro [catastro]
  (j/insert! postgres :catastrosnew catastro))
 ;(j/insert! postgres :catastros catastro))

(defn update-catastro [catastro]
  (j/update! postgres :catastrosnew catastro ["account = ?" (:account catastro)]))
  ;(j/update! postgres :catastros catastro ["account = ?" (:account catastro)]))

;; Hay llaves foraneas con el id de sepomex

;; hay 132133 sepomex distintos, pero solo 129645 sepomex con distinto colony y zipcode
;; Dwelling key
