(ns consul.ficha
  (:require [net.cgrand.enlive-html :as html]))

(defn get-url [url]
  (html/html-resource (java.net.URL. url)))

                                        ; "http://ciudadmx.cdmx.gob.mx:8080/seduvi/fichasReporte/fichaInformacion.jsp?nombreConexion=cCuajimalpa&cuentaCatastral=056_884_61&idDenuncia=&ocultar=1&x=-99.2694399356&y=19.3523980448&z=0.5"

(defn parse-tds [url]
 (html/select (html/html-resource (java.net.URL. url)) [:table :td]))
