(def cdmx-uso-de-suelo
  {:type "Feature",
 :geometry
 {:type "Point", :coordinates [-99.2694399356 19.3523980448]},
 :properties
 {:calle "PASEO TOLSA",
  :colonia "SANTA FE CUAJIMALPA",
  :cuenta-catastral "056_884_61",
  :liga-ciudadmx
  "http://ciudadmx.cdmx.gob.mx:8080/seduvi/fichasReporte/fichaInformacion.jsp?nombreConexion=cCuajimalpa&cuentaCatastral=056_884_61&idDenuncia=&ocultar=1&x=-99.2694399356&y=19.3523980448&z=0.5",
  :geopoint [19.3523980448 -99.2694399356],
  :longitude "-99.2694399356",
  :coordenadas "-99.2694399356, 19.3523980448",
  :altura "NULL",
  :alcaldia "CUAJIMALPA",
  :codigo-postal "05348",
  :superficie 1254.8651275,
  :densidad-descripcion "(1) Máximo de vivienda por conjunto",
  :uso-descripcion "Habitacional",
  :no-externo 503,
  :latitude "19.3523980448",
  :minimo-vivienda "NULL",
  :niveles "3",
  :area-libre "50"}})

(def maria-sepomex
  {:roads nil,
   :colony "San Angel",
   :description nil,
   :seo_url "san-angel-df",
   :id_polygon 1,
   :near_cities
   "{\"ids\":[\"136167\",\"136174\",\"136619\",\"136166\",\"136177\",\"136173\",\"136168\"],\"data_cities\":{\"136167\":{\"id\":\"136167\",\"city_id\":\"4\",\"state_id\":\"1\",\"name_city\":\"Coyoac\\u00e1n\",\"name_state\":\"Distrito Federal\",\"seo_url\":\"coyoacan\"},\"136174\":{\"id\":\"136174\",\"city_id\":\"11\",\"state_id\":\"1\",\"name_city\":\"Miguel Hidalgo\",\"name_state\":\"Distrito Federal\",\"seo_url\":\"miguel-hidalgo\"},\"136619\":{\"id\":\"136619\",\"city_id\":\"719\",\"state_id\":\"15\",\"name_city\":\"Ocoyoacac\",\"name_state\":\"M\\u00e9xico\",\"seo_url\":\"ocoyoacac\"},\"136166\":{\"id\":\"136166\",\"city_id\":\"3\",\"state_id\":\"1\",\"name_city\":\"Benito Ju\\u00e1rez\",\"name_state\":\"Distrito Federal\",\"seo_url\":\"benito-juarez-df\"},\"136177\":{\"id\":\"136177\",\"city_id\":\"14\",\"state_id\":\"1\",\"name_city\":\"Tlalpan\",\"name_state\":\"Distrito Federal\",\"seo_url\":\"tlalpan\"},\"136173\":{\"id\":\"136173\",\"city_id\":\"10\",\"state_id\":\"1\",\"name_city\":\"La Magdalena Contreras\",\"name_state\":\"Distrito Federal\",\"seo_url\":\"la-magdalena-contreras\"},\"136168\":{\"id\":\"136168\",\"city_id\":\"5\",\"state_id\":\"1\",\"name_city\":\"Cuajimalpa de Morelos\",\"name_state\":\"Distrito Federal\",\"seo_url\":\"cuajimalpa-de-morelos\"}}}",
   :history_image nil,
   :img_polygon 1,
   :historic_place_image nil,
   :cover_image nil,
   :city "Álvaro Obregón",
   :history nil,
   :state "Distrito Federal",
   :zipcode "01000",
   :metro_rail nil,
   :breadcrumb_seo_url
   "{\"DF\":\"df\",\"\\u00c1lvaro Obreg\\u00f3n\":\"alvaro-obregon-df\",\"San Angel\":\"san-angel-df\"}",
   :bus_routes nil,
   :cve_city "010",
   :habitability_score_global nil,
   :id 138393,
   :state_url "df",
   :lon -99.1912897,
   :img_mini_polygon 1,
   :near_colonies
   "{\"tooltips\":{\"1\":[\"Guadalupe Inn\",\"Florida\",\"Axotla\",\"Campestre\",\"Tlacopac\",\"Ex-Hacienda de Guadalupe Chimalistac\",\"San Angel Inn\",\"Altavista\",\"Chimalistac\",\"Progreso Tizapan\",\"Tizapan\",\"Loreto\",\"La Otra Banda\",\"Barrio Oxtopulco Universidad\",\"Fort\\u00edn de Chimalistac\",\"Torres de Chimalistac\",\"Copilco Universidad ISSSTE\",\"Insurgentes San Angel\",\"Copilco El Bajo\",\"Copilco El Alto\",\"Copilco Universidad\"],\"2\":[\"Los Alpes\",\"Ermita Tizapan\",\"Merced G\\u00f3mez\",\"Sociedad Cooperativa Uni\\u00f3n Poder Popular\",\"Ampliaci\\u00f3n Alpes\",\"Las \\u00c1guilas\",\"Ampliaci\\u00f3n Las Aguilas\",\"La Herradura del Pueblo Tetelpan\",\"Atlamaya\",\"Flor de Maria\",\"Olivar de los Padres\",\"Tizampampano del Pueblo Tetelpan\",\"Lomas de los Angeles del Pueblo Tetelpan\",\"Lomas de San \\u00c1ngel Inn\",\"Jardines del Pedregal\",\"San Jos\\u00e9 Insurgentes\",\"Merced G\\u00f3mez\",\"Cr\\u00e9dito Constructor\",\"Villa Coyoac\\u00e1n\",\"Barrio Santa Catarina\",\"Del Carmen\",\"Romero de Terreros\",\"Monte de Piedad\",\"Altillo Universidad\",\"Copilco\",\"Villas Copilco\",\"Acasulco\",\"Integraci\\u00f3n Latinoamericana\",\"Empleados Federales\",\"Unidad Independencia IMSS\"],\"3\":[\"Las \\u00c1guilas\",\"Hogar y Redenci\\u00f3n\",\"Ca\\u00f1ada del Olivar\",\"Batall\\u00f3n de San Patricio\",\"Unidad Popular Emiliano Zapata\",\"Alfonso XIII\",\"Molino de Rosas\",\"Alfalfar\",\"Lomas de Plateros\",\"La Cascada\",\"Torres de Mixcoac\",\"Lomas de Tarango\",\"Las Aguilas 1a Secci\\u00f3n\",\"Las Aguilas 2o Parque\",\"Las Aguilas 3er Parque\",\"Miguel Hidalgo\",\"Del Valle Sur\",\"Actipan\",\"Acacias\",\"Xoco\",\"Mixcoac\",\"Insurgentes Mixcoac\",\"Viveros de Coyoac\\u00e1n\",\"Romero de Terreros\",\"Cuadrante de San Francisco\",\"Pedregal de San Francisco\",\"Dr. Pedro R Bernab\\u00e9\",\"Pedregal de Santo Domingo\",\"Puente Sierra\",\"San Jer\\u00f3nimo L\\u00eddice\"]},\"results\":{\"1\":\"3,6,7,8,10,11,12,13,14,15,17,18,19,490,491,492,509,510,511,518,519\",\"2\":\"2,3,6,7,8,10,11,12,13,14,15,16,17,18,19,189,190,219,220,231,232,233,234,237,238,241,242,256,454,457,458,460,462,466,487,488,490,491,492,509,510,511,512,513,514,515,516,517,518,519,1367\",\"3\":\"2,3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,133,134,135,136,137,138,139,140,141,142,189,190,197,219,220,228,229,230,231,232,233,234,237,238,240,241,242,256,401,408,409,416,454,455,456,457,458,460,462,466,467,487,488,490,491,492,493,494,495,496,509,510,511,512,513,514,515,516,517,518,519,521,1367,1368,1370\"}}",
   :should_visit_image nil,
   :lat 19.3465231,
   :habitability_score_leisure nil,
   :zone_id 150012,
   :rtp_routes nil,
   :habitability_score_infrastructure nil,
   :state_label "DF",
   :historic_place nil,
   :city_id 1,
   :habitability_score_economy nil,
   :habitability_score_dwelling nil,
   :trolley_routes nil,
   :should_visit nil,
   :state_id 1})

(def poligonos-predios {
 :gid 930126,
 :name "1151538",
 :descriptio nil,
 :geom
 "MULTIPOLYGON (((-99.100153 19.268904, -99.100149 19.268912, -99.100138 19.268938, -99.100095 19.268918, -99.100082 19.268911, -99.100053 19.268898, -99.100041 19.268892, -99.100038 19.268891, -99.10005 19.268861, -99.100117 19.268889, -99.100153 19.268904)))"})

(def catastros
  {:colony "COVE",
 :polygon_id "1",
 :feasibilities
 "[[\"\", \"\"], [\"Tipos de terreno para conexi\\u00f3n de servicios de agua y drenaje (Art. 202 y 203 C\\u00f3digo Financiero)\", \"\"], [\"Zona de Impacto Vial (Art. 319 C\\u00f3digo Financiero)\", \"\\u00a0\"], [\"\", \"\\u00a0\"], [\"\", \".\"], [\"\", \"\\u00a0\"], [\"Tipos de terreno para conexi\\u00f3n de servicios de agua y drenaje (Art. 202 y 203 C\\u00f3digo Financiero)\", \"\\u00a0\"], [\"Zona de Impacto Vial (Art. 319 C\\u00f3digo Financiero)\", \"\"]]",
 :list_norms
 "[[\"actuacion\", \"A08\"], [\"generales\", \"01\"], [\"generales\", \"03\"], [\"generales\", \"04\"], [\"generales\", \"08\"], [\"generales\", \"11\"], [\"generales\", \"13\"], [\"generales\", \"17\"], [\"generales\", \"29\"], [\"particulares\", \"01\"], [\"particulares\", \"02\"], [\"particulares\", \"10\"]]",
 :min_dewlling 0.0,
 :correct_latitude "19.4013462",
 :number "297",
 :sepomex_id 32,
 :city "ALVARO OBREGON",
 :correct_longitude "-99.2019358",
 :linked nil,
 :longitude "-99.20190299999999",
 :image_polygon nil,
 :state nil,
 :zipcode "01120",
 :land_use "Habitacional.",
 :max_dewlling 3.0,
 :size "160.0",
 :free_area 30.0,
 :street "OBSERVATORIO",
 :account "03735401",
 :levels 3.0,
 :max_construction 335.0,
 :centroid
 #object[org.postgresql.util.PGobject 0xfefabd7 "0101000020E61000009B1F5A95ECCC58C05A66618ABD663340"],
 :id 10730,
 :latitude "19.401379",
 :point
 #object[org.postgresql.util.PGobject 0x2077728c "0101000020E6100000B67D8FFAEBCC58C01A4D2EC6C0663340"],
 :result_set_sepomex 1,
 :city_id 1,
 :density "M(Media, 1 Viv C/ 50 m2)",
 :on_amazon 0,
 :polygon_url "",
 :height "0.0",
 :polygon
 #object[org.postgresql.util.PGobject 0x5824e995 "0106000020E6100000010000000103000000010000000D000000B0726891EDCC58C0CC7B9C69C2663340994A3FE1ECCC58C04F5AB8ACC266334069E04735ECCC58C073309B00C36633402234828DEBCC58C0A7203F1BB9663340DA39CD02EDCC58C0FBAE08FEB766334063EFC517EDCC58C0ADDD76A1B96633404BADF71BEDCC58C030BC92E4B9663340336B2920EDCC58C0B39AAE27BA663340CE8B135FEDCC58C04548DDCEBE663340B6494563EDCC58C0691EC022BF6633403FFF3D78EDCC58C07A5567B5C06633403FFF3D78EDCC58C07A5567B5C0663340B0726891EDCC58C0CC7B9C69C2663340"],
 :state_id nil})
